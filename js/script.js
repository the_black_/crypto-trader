/**
 * Created by Isti on 2018. 10. 06..
 */
var currencyTypes=['btc', 'eth', 'xrp'];
var exchangeTable; // html-be lévő táblázat
var btcHistory;
var ethHistory;
var xrpHistory;

var configJSON;

var balanceInit = false;
var config;
var dataForChart1;
var dataForChart2;
var dataForChart3;

var datesForChart1;

var ctx;

window.addEventListener("load", function () {
    readConfigJSON();
    exchangeTable = document.getElementById("tableTrends");
    ctx = document.getElementById('canvas').getContext('2d');

    $('#btnBuy').click(clickOnBuy);
    $('#btnSell').click(clickOnSell);
    $('#btnReset').click(postReset);
    $('#btnCoinsToSell').click(clickOnSellByID);

    //dark mode from cookie (localStorage)
    if (localStorage["currentStyle"]=="night")
    {
        switch_style('night');
    }
    else {
        switch_style('light');
    }
});

function readMyPending(){
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function() {
        //offerTableBuilder('tableOffers',this.response, false);
        if(this.response.length > 0){
            validateMyPendingOneByOne('tableOffers', this.response, false);
        }
    });
    oReq.responseType="json";
    oReq.open("GET", "https://csapataska.azurewebsites.net/api/options/pending");
    oReq.send();
}

function readOthersPending(teamID){
    console.log("others pending incomming: " + teamID + configJSON.Pending);
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function() {
        //offerTableBuilder('tableOffers',this.response, false);
        if(this.response.length > 0){
            validateMyPendingOneByOne('IDOptionsTable', this.response, true);
        }

    });
    oReq.responseType="json";
    oReq.open("GET", teamID + configJSON.Pending);
    oReq.send();
}

function validateMyPendingOneByOne(tableID, json, toSellOrNotToSell) {
    console.log(json);
    for(var i = 0; i < json.length; i++){
        val(json[i].id, tableID, json[i], toSellOrNotToSell);
    }

}

function val(currencyID, tableID, oneJson, toSellOrNotToSell){
    var oReq = new XMLHttpRequest();
    oReq.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("SValidálás lehetséges, erre: ");
            console.log(this.response);
            console.log(this.response.rate + " " + oneJson.rate + " "+ this.response.amount +" " + oneJson.amount + this.response.symbol + oneJson.symbol);
            if(this.response.rate == oneJson.rate && this.response.amount == oneJson.amount && this.response.symbol == oneJson.symbol){
                myOfferBuilder(tableID, this.response, toSellOrNotToSell);
              console.log("validálás sikeres" + oneJson.id);
            }
        }
    }
    oReq.responseType="json";
    oReq.open("GET", configJSON.ValidateOthersCurrencyURL + currencyID);
    oReq.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    oReq.send();
}

function refreshMyOffers(tableID){
    var tbl = document.getElementById(tableID);   // Get the table
    if(tbl.childElementCount == 2){
        tbl.removeChild(tbl.getElementsByTagName("tbody")[0]); // Remove first instance of body
    }
}

function readConfigJSON(){
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function () {
        configJSON = this.response;
        //console.log("KONFIG");
        //console.log((JSON).parse(JSON.stringify(this.response)));
        //console.log(configJSON);
        getIDSellerOptions();
        getBalance();
        currencyTypes.forEach(function (element) {
            getExchangeRate(element);
        });
        getHistory();
        readMyPending();
    });
    oReq.responseType="json";
    oReq.open("GET", "config.json");
    oReq.send();
}

function getIDSellerOptions() {// betölti a csapatneveket a legördulő listába
    var sellerSelect = document.getElementById('selectTeam');

    var teamCount = Object.keys(configJSON.TeamSites).length;
    for (var i = 0; i<teamCount; i++) {
        var newOption = document.createElement('option');
        var optionText = document.createTextNode(Object.keys(configJSON.TeamSites)[i]);

        //if (Object.keys(configJSON.TeamSites)[i].toString() !== "csapataska")
        //{
            newOption.appendChild(optionText);
            sellerSelect.appendChild(newOption);
        //}
    }
}

function clickOnIDSellerOption() {
    //console.log("OPTION CHANGED")
    var sellerSelect = document.getElementById('selectTeam');
    //console.log("selected option: "+ sellerSelect.options[sellerSelect.selectedIndex].value);
    var currentOption = sellerSelect.options[sellerSelect.selectedIndex].value;

    //console.log(configJSON.TeamSites[currentOption]);
    if (currentOption!=="Choose...") {
        console.log(currentOption + "Kiválasztva ");
        //getSelectedIDOption(configJSON.TeamSites[currentOption]);
        refreshMyOffers('IDOptionsTable');
        //refreshMyOffers('tableOffers');
        readOthersPending(configJSON.TeamSites[currentOption]);
    }
}

function resetTable(id) {
    var table = document.getElementById(id);
    while (table.hasChildNodes()) {
        console.log("törlés: " + table.firstChild);
        table.removeChild(table.firstChild);
    }
}

function myOfferBuilder(tableID, myJson, toSellOrNotToSell){
    var table = document.getElementById(tableID);
    console.log("Ez kerül ki a táblázatba:");
    console.log(myJson);
    console.log("tábla elemek száma: " + table.childElementCount);
    console.log(table.childNodes);
    if(table.childElementCount == 0){
        console.log("táblázat fej");
        var thead = document.createElement('thead');
        var row = document.createElement('tr');

        //fejlec megcsinalasa
        for (var j=1; j<Object.keys(myJson).length; j++) {
            var text = document.createTextNode(Object.keys(myJson)[j]);
            var cell = document.createElement('th');

            cell.appendChild(text);
            row.appendChild(cell);
        }

        thead.appendChild(row);
        table.appendChild(thead);
    }
    if(table.childElementCount == 1) {
        var tbody = document.createElement('tbody');
        table.appendChild(tbody);
    }
    if(table.childElementCount >= 0){
        console.log("táblázat tartalom");
        // tartalommal feltöltés
        console.log("táblázat body");
        var tbody = table.childNodes[2];
        var row = document.createElement('tr');
        for (var i=1; i<Object.keys(myJson).length; i++){
            var text = document.createTextNode(myJson[Object.keys(myJson)[i]]);
            var cell = document.createElement('td');

            cell.appendChild(text);
            row.appendChild(cell);

            tbody.appendChild(row);
        }
        console.log("toSellOrNotTOSell: " + toSellOrNotToSell);
        if (toSellOrNotToSell==true)
        {
            console.log("Lehet vásárolni");
            text = document.createTextNode("Vásárlás");
            var button = document.createElement('button');
            button.setAttribute("id", myJson.id); // minden ELADÁS gombnak van egy ID-ja, ez az ID az eladó valuta ID-ja
            button.addEventListener("click", function () {
                buyOthersCurrency(this.getAttribute("id"));
            });
            button.className = 'btn btn-warning';
            cell = document.createElement('td');

            button.appendChild(text);
            cell.appendChild(button);
            row.appendChild(cell);
        }
        tbody.appendChild(row);
    }
    new Tablesort(document.getElementById(tableID));
}

function buyOthersCurrency(id) {
    var p = new XMLHttpRequest();
    p.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert("Vásárlás sikeres");
            getBalance();

        }
        else if(this.readyState == 4 && this.status != 200){
            alert("A vásárlás folyamán hiba történtt!");
        }
    };
    console.log("vásárlás: " + configJSON.ValidateOthersCurrencyURL + id + "/" +"purchase");
    p.open("POST",  configJSON.ValidateOthersCurrencyURL + id + "/" +"purchase");
    p.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    p.setRequestHeader("Content-Type", "application/json");
    p.send();
}

function reqListenerBalance() {
    document.getElementById('myCash').innerHTML = JSON.parse(JSON.stringify(this.response)).usd.toFixed(4) + " " + Object.keys(JSON.parse(JSON.stringify(this.response)))[1].toUpperCase();;

    //var balance = document.getElementById('balanceDiv');

    var columnCount = Object.keys(this.response).length - 2;

    var currencies = ["Bitcoin", "Etherum", "Ripple"];

    var balanceDiv = document.getElementById('balance');
    var table = document.createElement('table');
    var tableBody = document.createElement('tbody');
    var row = document.createElement('tr');
    for ( var i= 0; i<columnCount; i++)
    {
        var cell = document.createElement('th');
        var cellText = document.createTextNode(currencies[i]);

        cell.appendChild(cellText);
        row.appendChild(cell);
    }

    //1. sor "mentese"
    tableBody.appendChild(row);

    //2. sor letrehozasa
    var row = document.createElement('tr');

    var cell = document.createElement('td');
    cell.id = "0";
    var cellText = document.createTextNode(JSON.parse(JSON.stringify(this.response)).btc);

    cell.appendChild(cellText);
    row.appendChild(cell);

    var cell = document.createElement('td');
    cell.id = "1";
    var cellText = document.createTextNode(JSON.parse(JSON.stringify(this.response)).eth);

    cell.appendChild(cellText);
    row.appendChild(cell);

    var cell = document.createElement('td');
    cell.id = "2";
    var cellText = document.createTextNode(JSON.parse(JSON.stringify(this.response)).xrp);

    cell.appendChild(cellText);
    row.appendChild(cell);

    //2. sor "mentese"
    tableBody.appendChild(row);
    table.appendChild(tableBody);
    balanceDiv.appendChild(table);

    balanceInit = true;
}

function reqListenerBalanceUpdate() {

    document.getElementById('myCash').innerHTML = JSON.parse(JSON.stringify(this.response)).usd.toFixed(4) + " " + Object.keys(JSON.parse(JSON.stringify(this.response)))[1].toUpperCase();;

    var cellToUpdate = document.getElementById("0");
    cellToUpdate.innerHTML = JSON.parse(JSON.stringify(this.response)).btc;

    var cellToUpdate = document.getElementById("1");
    cellToUpdate.innerHTML = JSON.parse(JSON.stringify(this.response)).eth;

    var cellToUpdate = document.getElementById("2");
    cellToUpdate.innerHTML = JSON.parse(JSON.stringify(this.response)).xrp;

    getHistory();

    //console.log("shits updated");
}

function getBalance() {
    var oReq = new XMLHttpRequest();
    if (!balanceInit)
    {
        oReq.addEventListener("load", reqListenerBalance);
    }
    else
    {
        oReq.addEventListener("load", reqListenerBalanceUpdate);
    }

    oReq.responseType="json";

    oReq.open("GET", configJSON.MainBaseURL + "/api/account");
    oReq.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    oReq.send();
}

function reqListenerHistory() {

    var history = JSON.parse(JSON.stringify(this.response));

    resetTable('tableHistoryBody');

    var tableHistoryBosy = document.getElementById('tableHistoryBody');

    //console.log("history count: " + Object.keys(history).length);
    for (var i=0; i<Object.keys(history).length; i++)
    {

        if (history[i].symbol != null)
        {
            var tableRow = document.createElement('tr');

            var tableCell = document.createElement('td');
            var cellText = document.createTextNode(history[i].symbol);
            tableCell.appendChild(cellText);
            tableRow.appendChild(tableCell);

            var tableCell = document.createElement('td');
            var cellText = document.createTextNode(history[i].amount);
            tableCell.appendChild(cellText);
            tableRow.appendChild(tableCell);

            var tableCell = document.createElement('td');
            var cellText = document.createTextNode(history[i].type);
            tableCell.appendChild(cellText);
            tableRow.appendChild(tableCell);

            var tableCell = document.createElement('td');
            var cellText = document.createTextNode(history[i].createdAt);
            tableCell.appendChild(cellText);
            tableRow.appendChild(tableCell);

            var tableCell = document.createElement('td');
            var cellText;
            if (history[i].symbol.toString() == 'BTC')
            {
                cellText = document.createTextNode(history[i].exchangeRates.btc);
            }
            else if (history[i].symbol.toString() == 'ETH')
            {
                cellText = document.createTextNode(history[i].exchangeRates.eth);
            }
            else
            {
                cellText = document.createTextNode(history[i].exchangeRates.xrp);
            }

            tableCell.appendChild(cellText);
            tableRow.appendChild(tableCell);
            tableHistoryBosy.appendChild(tableRow);
            //tableHistory.appendChild(tableRow);
        }
        else
        {

                var tableRow = document.createElement('tr');

                var tableCell = document.createElement('td');
                var cellText = document.createTextNode("-");
                tableCell.appendChild(cellText);
                tableRow.appendChild(tableCell);

                var tableCell = document.createElement('td');
                var cellText = document.createTextNode("-");
                tableCell.appendChild(cellText);
                tableRow.appendChild(tableCell);

                var tableCell = document.createElement('td');
                var cellText = document.createTextNode(history[i].type);
                tableCell.appendChild(cellText);
                tableRow.appendChild(tableCell);

                var tableCell = document.createElement('td');
                var cellText = document.createTextNode(history[i].createdAt);
                tableCell.appendChild(cellText);
                tableRow.appendChild(tableCell);

                var tableCell = document.createElement('td');
                var cellText = document.createTextNode("-");


                tableCell.appendChild(cellText);
                tableRow.appendChild(tableCell);
            tableHistoryBosy.appendChild(tableRow);
            //tableHistory.appendChild(tableRow);

        }
    }
    new Tablesort(document.getElementById('tableHistory'));
}

function getHistory() {
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListenerHistory);
    oReq.responseType="json";

    oReq.open("GET",  configJSON.MainBaseURL + "/api/account/history");
    oReq.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    oReq.send();
}

function drawCharts() {
    console.log("kesobbi");

    config = {
        type: 'line',
        data: {
            labels: datesForChart1,
            datasets: [{
                label: 'Bitcoin',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: dataForChart1,
                fill: false,
            }, {
                label: 'Etherum',
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: dataForChart2,
            }, {
                label: 'Ripple',
                fill: false,
                backgroundColor: window.chartColors.yellow,
                borderColor: window.chartColors.yellow,
                data: dataForChart3,
            }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: false,
                text: 'title'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Időpont'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Érték'
                    }
                }]
            }
        }
    };
}


function reqExchangeListener() {
    console.log("reqExchangeListener");
    /*  ____________________
        |        |   BTC   |
        |változás|---------|
        |________|___6601__|

     */
    var row1 = document.createElement('tr');
    var row2 = document.createElement('tr');
    var tdChange = document.createElement('td');
    var tdCurrency = document.createElement('td');
    var tdValue = document.createElement('td');
    tdChange.className = 'text-center'
    tdCurrency.className ='h2 text-center';
    tdValue.className ='text-muted text-center';

    var actualCurrency = JSON.parse(JSON.stringify(this.response)).symbol; // aktuálisan vizsgált valuta pl.: BTC
    var arrowHeading;
    /*
        arrowHeading = 1 , ha nőtt
        arrowHeading = 0 , ha nem változott
        arrowHeading = -1 , ha csökkent
     */
    var imgTrending = document.createElement('img');

    switch(actualCurrency){
        case "BTC":
            btcHistory = JSON.parse(JSON.stringify(this.response)).history;
            var dates = Object.keys(btcHistory);
            console.log("BTC history dates: \n"+dates);
            dataForChart1=[];
            for ( var i =  0; i < dates.length; i++)
            {
                dataForChart1.push(parseFloat(btcHistory[dates[i]]));
                //console.log("dataForChart1:" + dataForChart1[i]);
            }
            console.log(actualCurrency + " chart data: \n" + dataForChart1);
            //dataForChart1=makeAverageData(dataForChart1); // atlagolt 1. adatsor

            arrowHeading = getArrowHeading(btcHistory);
            break;
        case "ETH":
            ethHistory = JSON.parse(JSON.stringify(this.response)).history;
            var dates = Object.keys(ethHistory);
            console.log("ETH history dates: \n"+dates);
            dataForChart2=[];
            for ( var i =  0; i<dates.length; i++)
            {
                dataForChart2.push(parseFloat(ethHistory[dates[i]]));
                //console.log("dataForChart2:" + dataForChart2[i]);
            }
            console.log(actualCurrency + " chart data: \n" + dataForChart2);
           // dataForChart2=makeAverageData(dataForChart2); // atlagolt 2. adatsor

            arrowHeading = getArrowHeading(ethHistory);
            break;
        case "XRP":
            xrpHistory = JSON.parse(JSON.stringify(this.response)).history;
            var dates = Object.keys(xrpHistory);
            console.log("XRP history dates: \n"+dates);
            dataForChart3=[];
            for ( var i =  0; i<dates.length; i++)
            {
                dataForChart3.push(parseFloat(xrpHistory[dates[i]]));
                //console.log("dataForChart3:" + dataForChart3[i]);
            }
            console.log(actualCurrency + " chart data: \n" + dataForChart3);
            //dataForChart3=makeAverageData(dataForChart3); //atlagolt 3. adatsor

            arrowHeading = getArrowHeading(xrpHistory);
            break;
    }

    var last24Hours = [];
    var index24=0;
    for (var i = 0; i < dates.length; i++)// ide esetleg nem artana ha nem lenne beleegetve a 12, meg egyeb helyeken se
    {
        last24Hours[index24]=dates[i].toString().split(" ")[1];
        index24=index24+1;
    }
    console.log("last24Hours: \n" + last24Hours)

    datesForChart1=last24Hours;//datum labelek

    var today = new Date(dates[0]); // pl: "2018-10-13 12:25:00"
    var yesterday;
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);            // ekorra már így néz ki: "2018-10-13 0:0:0"
    console.log("ma: " + today);
    for (var i = 0; i < dates.length; i++) {
        if(new Date(dates[i]).getTime() < today.getTime()){
            yesterday = dates[i]; // legutolsó tegnapi dátum
            break;
        }
    }

    drawCharts(); // grafikon rajzolasa
    window.myLine = new Chart(ctx, config);

    switch(arrowHeading){
        case 1:
            imgTrending.src = "pics/up.png";
            break;
        case 0:
            imgTrending.src = "pics/noChange.png";
            break;
        case-1:
            imgTrending.src = "pics/down.png";
            break;
    }

    tdChange.appendChild(imgTrending);//document.createTextNode(arrowHeading));
    tdChange.setAttribute("rowspan","2");
    row1.appendChild(tdChange);

    tdCurrency.appendChild(document.createTextNode(actualCurrency));
    row1.appendChild(tdCurrency);

    tdValue.appendChild(document.createTextNode(JSON.parse(JSON.stringify(this.response)).currentRate.toFixed(4)));
    row2.appendChild(tdValue);

    exchangeTable.appendChild(row1);
    exchangeTable.appendChild(row2);
}

function getDisplayedDataCountForChart(count){ // meghatározza hány adat jelenjen meg a chart-on
    console.log("count kapott:" + count);
    if(count <= 25){
        return count;
    }
    else{
        return 25;
    }
}

function getArrowHeading(history) {
    var dates = Object.keys(history);
    var lastData = dates[0];
    console.log("legkorábbi adat: " + lastData + " " + history[lastData] + " most: " + history[dates[dates.length-1]]);
    if(parseFloat(history[lastData]) > parseFloat(history[dates[dates.length-1]])){
        console.log("csokkent");
        return -1;
    }
    else if(parseFloat(history[dates[0]]) < parseFloat(history[dates[dates.length-1]])){
        console.log("nott");
        return 1;
    }
    else {
        console.log("nem valtozott");
        return 0;
    }
}

function getExchangeRate(currency) {
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqExchangeListener);
    oReq.responseType="json";

    oReq.open("GET",  configJSON.MainBaseURL + "/api/exchange/"+currency);
    oReq.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    oReq.send();
}

function clickOnBuy(){
    var amount = document.getElementById('textAmount').value;
    var dropDown = document.getElementById('inputGroupSelect04'); // <----------------- jó lenne innen feltölteni a dropdown-t a currencyTypes-al
    var selectedCurrency = dropDown.options[dropDown.selectedIndex].text;
    if(confirm("Biztosan megvesz " + amount + " " + selectedCurrency + "-t ?")){
        postBuy(selectedCurrency, amount);
    }
}

function clickOnSell(){
    var amount = document.getElementById('textAmount').value;
    var dropDown = document.getElementById('inputGroupSelect04'); // <----------------- jó lenne innen feltölteni a dropdown-t a currencyTypes-al
    var selectedCurrency = dropDown.options[dropDown.selectedIndex].text;
    if(confirm("Biztosan elad " + amount + " " + selectedCurrency + "-t ?")){
        postSell(selectedCurrency, amount);
    }
}

function clickOnSellByID(){ // lehet jó lenne valahogy tömbbe tárolni a valuták ID-t, mert így új valuta esetén sokat kell másolgatni
    console.log("clickonSellById");
    var btcValueString = document.getElementById('btcToSell').value; // itt még bármi lehet az érték szám és betű is és 0 is
    var ethValueString = document.getElementById('ethToSell').value; // itt még bármi lehet az érték szám és betű is és 0 is
    var xrpValueString = document.getElementById('xrpToSell').value; // itt még bármi lehet az érték szám és betű is és 0 is

    var btcValue = isItNumber(btcValueString)? Number(btcValueString) : 0; // ha szám a string akkor számmá alakítja, ha nem akkor 0 lesz
    var ethValue = isItNumber(ethValueString)? Number(ethValueString) : 0; // ha szám a string akkor számmá alakítja, ha nem akkor 0 lesz
    var xrpValue = isItNumber(xrpValueString)? Number(xrpValueString) : 0; // ha szám a string akkor számmá alakítja, ha nem akkor 0 lesz

    var btcUSDString = document.getElementById('btcUSD').value; // itt még bármi lehet az érték szám és betű is és 0 is
    var ethUSDString = document.getElementById('ethUSD').value; // itt még bármi lehet az érték szám és betű is és 0 is
    var xrpUSDString = document.getElementById('xrpUSD').value; // itt még bármi lehet az érték szám és betű is és 0 is

    var btcUSD = isItNumber(btcValueString)? Number(btcUSDString) : 0; // ha szám a string akkor számmá alakítja, ha nem akkor 0 lesz
    var ethUSD= isItNumber(ethValueString)? Number(ethUSDString) : 0; // ha szám a string akkor számmá alakítja, ha nem akkor 0 lesz
    var xrpUSD = isItNumber(xrpValueString)? Number(xrpUSDString) : 0; // ha szám a string akkor számmá alakítja, ha nem akkor 0 lesz

    if(!(xrpValue === 0) && !(xrpUSD === 0)){
        if(confirm("Biztosan elad " + xrpValue + "XRP-t " + xrpUSD + "$-t ?")){
            postSellToOthers('XRP', xrpValue, xrpUSD);
        }
    }

    if(!(ethValue === 0) && !(ethUSD === 0)){
        if(confirm("Biztosan elad " + ethValue + "ETH-t " + ethUSD + "$-t ?")){
            postSellToOthers('ETH', ethValue, ethUSD);
        }
    }

    if(!(btcValue === 0) && !(btcUSD === 0)){
        if(confirm("Biztosan elad " + ethValue + "BTC-t " + btcUSD + "$-t ?")){
            postSellToOthers('BTC', btcValue, btcUSD);
        }
    }

    if((btcValue === 0) && (btcUSD === 0) && (xrpValue === 0) && (xrpUSD === 0) && (ethValue === 0) && (ethUSD === 0)){
        alert("Így nem lehet eladásra kínálni!");
    }

}

function phpToJSjson(json){
    console.log("json from php:");
    /*-----------------------------Minta adatok-------------------------------

    json = {"data":[{"ID":1,"Date":{"date":"2018-10-21 00:00:00.000000","timezone_type":3,"timezone":"America/Los_Angeles"},"Temperature":25.700000762939,"Humidity":59},{"ID":2,"Date":{"date":"2007-05-08 00:00:00.000000","timezone_type":3,"timezone":"America/Los_Angeles"},"Temperature":25.700000762939,"Humidity":59},{"ID":3,"Date":{"date":"2018-10-21 00:00:00.000000","timezone_type":3,"timezone":"America/Los_Angeles"},"Temperature":25.700000762939,"Humidity":59},{"ID":4,"Date":{"date":"2018-10-21 00:00:00.000000","timezone_type":3,"timezone":"America/Los_Angeles"},"Temperature":25.60000038147,"Humidity":65.099998474121},{"ID":5,"Date":{"date":"2018-10-21 21:35:00.000000","timezone_type":3,"timezone":"America/Los_Angeles"},"Temperature":30.200000762939,"Humidity":49}]}

    {
        "ID": 1,
        "Date": {
            "date": "2018-10-21 00:00:00.000000",
            "timezone_type": 3,
            "timezone": "America/Los_Angeles"
             },
        "Temperature": "25.7",
        "Humidity": "59.0"
    },
    -------------------------------------------------------------------------*/
    var myjson = JSON.parse(json);
    console.log(myjson);

}

function isItNumber(string){
    return !isNaN(string);
}

function getValidUntilString() {
    var now = new Date();
    now.setMonth(now.getMonth()+6);// 6 hónappal későbbig lesz jó
    var year = now.getFullYear();
    var month = now.getMonth();
    var day = now.getDate();
    var hour = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}

function postSellToOthers(currency, amount, rate){
    var validUntil = getValidUntilString();
    var p = new XMLHttpRequest();
    p.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //alert("Siker: " + this.response);
            alert("Valuta eladásra kínálása sikeresen megtörtént!");
            var offerID = JSON.parse(this.response.toString()).id;
            console.log(offerID);
            console.log("Siker volt a postSellToOthers-be. ID:" + offerID);
            var offer = {"id":offerID, "symbol":currency, "amount":amount, "rate": rate, "validUntil": validUntil};

            var http = new XMLHttpRequest();
            var url = "index.php";
            var params = "ALLES="+JSON.stringify(offer);
            http.open("POST", url, true);

            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.onreadystatechange = function() {
                if(http.readyState == 4 && http.status == 200) {
                    //alert("ALLES átadva a PHP-nak hogy bekerüljön az adatbázisba.");
                    refreshMyOffers('tableOffers');
                    readMyPending();
                }
            }
            http.send(params);
        }
        else if (this.readyState == 4 && this.status != 200){
            alert("Valuta eladásra kínálása során hiba történt!" + this.response.message);
            console.log("Hiba volt a postSellToOthers-be: " + this.response);
        }

    };
    p.open("POST",  configJSON.MainBaseURL + "/api/options");
    p.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    p.setRequestHeader("Content-Type", "application/json");
    console.log(JSON.stringify({"symbol":currency, "amount":amount, "rate": rate, "validUntil": validUntil}));
    p.send(JSON.stringify({"symbol":currency, "amount":amount, "rate": rate, "validUntil": validUntil}));
}

function postBuy(currency, amount){
    var p = new XMLHttpRequest();
    p.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert("Vásárlás sikeres");
            getBalance();
        }
        else if(this.readyState == 4 && this.status != 200){
            alert("A vásárlás folyamán hiba történtt!");
        }
    };
    p.open("POST",  configJSON.MainBaseURL + "/api/account/purchase");
    p.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    p.setRequestHeader("Content-Type", "application/json");
    p.send(JSON.stringify({"Symbol":currency, "Amount":amount}));
}

function postSell(currency, amount){
    var p = new XMLHttpRequest();
    p.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert("Eladás sikeres!");
            getBalance();
        }
        else if(this.readyState == 4 && this.status != 200){
            alert("Az eladás folyamán hiba történtt!");
        }
    };
    p.open("POST",  configJSON.MainBaseURL + "/api/account/sell");
    p.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    p.setRequestHeader("Content-Type", "application/json");
    p.send(JSON.stringify({"Symbol":currency, "Amount":amount}));
}

function postReset(){
    var p = new XMLHttpRequest();
    p.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            getBalance();
        }
    };
    p.open("POST",  configJSON.MainBaseURL + "/api/account/reset");
    p.setRequestHeader("X-Access-Token", configJSON.MyAPIkey);
    p.setRequestHeader("Content-Type", "application/json");
    p.send();
}