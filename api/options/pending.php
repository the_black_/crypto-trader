<?PHP
    header('Content-type:application/json;charset=utf-8');
    $username = getenv(SQLAZURECONNSTR_sqlUserName);
    $password = getenv(SQLAZURECONNSTR_sqlPassword);
    try {
        $conn = new PDO("sqlsrv:server = tcp:mycsapataskadb.database.windows.net,1433; Database = csapataskaDB", $username, $password);
        $conn->setAttribute(PDO::SQLSRV_ATTR_FETCHES_NUMERIC_TYPE, true);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }
    catch (PDOException $e) {
        echo "Error connecting to SQL Server.";
        die(print_r($e));
    }

    $connectionInfo = array("UID" => $username."@mycsapataskadb", "pwd" => $password, "Database" => "csapataskaDB", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
    $serverName = "tcp:mycsapataskadb.database.windows.net,1433";
    $conn = sqlsrv_connect($serverName, $connectionInfo);



    // ELLNÖRZÉS, HOGY SIKERES E A KAPCSOLAT
    /*if( $conn ) {
        echo "Connection established.<br />";
    }
    else {
        echo "Connection could not be established.<br />";
    }*/

    $tsql = "select id, symbol, amount, rate, FORMAT(validUntil, 'yyyy-MM-ddThh:mm:ss') as validUntil from Offers order by validUntil desc";
    $result2 = sqlsrv_query($conn, $tsql);
    if($result2 === false){
        echo 'hiba volt';
    }
    //print_r($result2);
    //echo 'Jonnek az adatok <br>';
    $result = array();
    while ($row = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
        $result[] = $row;
    }
    //var_dump($result);
    $jsonResult = json_encode($result);
    //$data = array('id' => "kibebaszottID", 'symbol' => 'BTC', 'amount' => 0.04, "rate" => 8000, "validUntil" => "2018-11-10T23:00:00+00:00");
    echo $jsonResult;?>