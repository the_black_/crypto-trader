<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Crypto Trader</title>
    <script src="js/script.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/Chart.bundle.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/changeTheme.js"></script>
    <script src="tablesort-gh-pages/src/tablesort.js"></script>
    <script src="tablesort-gh-pages/src/sorts/tablesort.date.js"></script>
    <script src="tablesort-gh-pages/src/sorts/tablesort.dotsep.js"></script>
    <script src="tablesort-gh-pages/src/sorts/tablesort.filesize.js"></script>
    <script src="tablesort-gh-pages/src/sorts/tablesort.monthname.js"></script>
    <script src="tablesort-gh-pages/src/sorts/tablesort.number.js"></script>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/4.2.5/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/4.2.5/jquery.bootstrap-touchspin.js"></script>

    <link rel="stylesheet" href="css/styles.css"/>
    <link rel="stylesheet" type="text/css" title="light" href="css/light-styles.css"/>
    <link rel="alternate stylesheet" type="text/css" title="night" href="css/night-styles.css">


</head>
<body class="d-flex flex-column">
<?php
if (isset($_POST['ALLES'])){  // egy ilyen van az ALLES-ben {"id":"29c45920-da3a-4104-88a4-427858a0b51f","symbol":"XRP","amount":0.5,"rate":500,"validUntil": "2019-4-9 23:30:6"}
    $alles = json_decode($_POST['ALLES']);
    var_dump($alles);
    echo ">".$alles->id;
    // ez az sql utasítás insert-hez: insert into Offers values('123456', 'BTC', 0.01, 7600, '2019-05-10 10:11:11');
    $offerID = "'".$alles->id."', ";
    $symbol = "'".$alles->symbol."', ";
    $amount = $alles->amount.", ";
    $rate = $alles->rate.", ";
    $until = "'".$alles->validUntil."')";

    $sql = "insert into offers values (".$offerID.$symbol.$amount.$rate.$until;
    $username = getenv(SQLAZURECONNSTR_sqlUserName);
    $password = getenv(SQLAZURECONNSTR_sqlPassword);
    try {
        $conn = new PDO("sqlsrv:server = tcp:mycsapataskadb.database.windows.net,1433; Database = csapataskaDB", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e) {
        echo "Error connecting to SQL Server.";
        die(print_r($e));
    }

    $connectionInfo = array("UID" => $username."@mycsapataskadb", "pwd" => $password, "Database" => "csapataskaDB", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
    $serverName = "tcp:mycsapataskadb.database.windows.net,1433";
    $conn = sqlsrv_connect($serverName, $connectionInfo);

    // ELLNÖRZÉS, HOGY SIKERES E A KAPCSOLAT
    /*if( $conn ) {
        echo "Connection established.<br />";
    }
    else {
        echo "Connection could not be established.<br />";
    }*/

    $result2 = sqlsrv_query($conn, $sql);
    //echo ">".$sql."<";
}
// TESZTELÉSHEZ
/*else{
    echo 'Nincs beallitva!';
}*/

?>
<div class="pos-f-t">
    <nav class="navbar navbar-dark navbar-toggleable-md bg-primary mb-0 leftSide">
        <h3 class="flex-grow-1 m-10 bd-highlight text-center text-md-left">CRYPTO TRADER</h3>
        <div class="d-none d-md-block">
            <div class="d-flex flex-column flex-md-row">
                <div class="d-flex flex-row-reverse">
                    <div class="info-balance d-flex flex-column flex-md-row align-items-center align-items-sm-left">
                        <div class="align-items-center">
                            <button id="btnReset" class="btn btn-danger m-3">Reset Balance</button>
                        </div>
                        <div class="d-flex flex-row flex-md-column">
                            <p class="font-weight-bold">Account Balance available: </p>
                            <p id="myCash"></p>
                        </div>
                    </div>

                </div>
                <form class="m-3">
                    <button class="btn btn-light" onclick="switch_style('light');return false;" name="theme" id="light"><img class="logo-image" src="pics/sun.png"/></button>
                    <button class="btn btn-dark" onclick="switch_style('night');return false;" name="theme" id="night"><img class="logo-image" src="pics/moon.png"/></button>
                </form>
            </div>
        </div>
        <div class="d-block d-md-none">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>
    <div class="collapse d-md-none" id="navbarToggleExternalContent">
        <div class="bg-primary p-4 text-center">
            <p class="font-weight-bold">Account Balance available: </p>
            <p id="myCash2">5000 USD</p>
            <button id="btnReset2" class="btn btn-danger mb-3">Reset Balance</button>
            <form>
                <button class="btn btn-light" onclick="switch_style('light');return false;" name="theme" id="light2"><img class="logo-image" src="pics/sun.png"/></button>
                <button class="btn btn-dark" onclick="switch_style('night');return false;" name="theme" id="night2"><img class="logo-image" src="pics/moon.png"/></button>
            </form>
        </div>
    </div>
</div>

<div class="d-flex flex-column align-items-center">
    <div class=" d-flex  flex-column rightSide align-items-center ">
        <h3 class="display-5 text-light">Áttekintő</h3>
        <div class="w-100 d-flex flex-column flex-md-row">
            <div class="w-100 d-flex flex-column">

                <div class="panel panel-primary">
                    <div class="panel-heading">Kriptovaluta egyenleg</div>
                    <div id="balance" class="panel-body"></div>
                </div>

                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner ">
                        <div class="item active">
                            <img class="d-block w-100" src="https://elevenews.com/wp-content/uploads/2018/03/Ripple.jpg" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Vegyél Ripple coint!</h5>
                                <p>Mert az most nagyon megy.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img class="d-block w-100" src="https://preneurx.com/wp-content/uploads/2018/05/Is-Ethereum-A-Security.jpg" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Vegyél Ethereumot!</h5>
                                <p>Hidd el jól jársz vele!</p>
                            </div>
                        </div>
                        <div class="item">
                            <img class="d-block w-100" src="https://www.securitycu.org/wp-content/uploads/2017/12/SecurityCU-Bitcoin.jpg" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Vegyél Bitcoint!</h5>
                                <p>Minden befektető ezt ajánlja.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img class="d-block w-100" src="https://www.cryptocoinfaqs.com/wp-content/uploads/2017/03/bitcoin-1.jpg" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Bízz bennünk!</h5>
                                <p>Velünk csak jól járhatsz.</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">Kriptovaluta vásárlás/eladás</div>
                    <div class="panel-body">
                        <div class="d-flex flex-row w-50 mb-3">
                            <h5 class="mr-3">Összeg:</h5>
                            <input id="textAmount" class="form-control stilo w-100">
                        </div>
                        <div class="input-group d-flex flex-row">
                            <select class="custom-select stilo h-100" id="inputGroupSelect04">
                                <option selected>Choose...</option>
                                <option value="1">BTC</option>
                                <option value="2">ETH</option>
                                <option value="3">XRP</option>
                            </select>
                            <div class="input-group-append h-100">
                                <button id="btnBuy" class="btn btn-warning" type="button">Vásárlás</button>
                                <button id="btnSell" class="btn btn-danger" type="button">Eladás</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="w-100 d-flex flex-column">

                <div class="panel panel-primary">
                    <div class="panel-heading">Kriptovaluta középárfolyam</div>
                    <div class="panel-body">
                        <table id="tableTrends"></table>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">ID-s Vásárlás</div>
                    <div class="panel-body d-flex flex-column">
                        <div class="d-flex flex-row mb-3 align-baseline">
                            <h5 class="mr-3">Eladó:</h5>
                            <select class="custom-select stilo h-100" id="selectTeam" onchange="clickOnIDSellerOption()">
                                <option selected>Choose...</option>
                            </select>
                        </div>
                        <div class="input-group d-flex flex-row">
                            <table id="IDOptionsTable">

                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">ID-s Eladás</div>
                    <div class="panel-body d-flex flex-column">
                        <div class="d-flex flex-sm-row flex-column">
                            <div class="d-flex flex-column w-100 mb-3">
                                <p>Bitcoin</p>
                                <input type="text" id="btcToSell" class="form-control stilo" name="spinner">
                                <p class="mt-3">Ár (USD/BTC)</p>
                                <input type="text" id="btcUSD" class="form-control stilo" name="spinner">
                            </div>
                            <div class="d-flex flex-column w-100 mb-3">
                                <p>Ethereum</p>
                                <input type="text" id="ethToSell" class="form-control stilo" name="spinner">
                                <p class="mt-3">Ár (USD/ETH)</p>
                                <input type="text" id="ethUSD" class="form-control stilo" name="spinner">
                            </div>
                            <div class="d-flex flex-column w-100 mb-3">
                                <p>Ripple</p>
                                <input type="text" id="xrpToSell" class="form-control stilo" name="spinner">
                                <p class="mt-3">Ár (USD/XRP)</p>
                                <input type="text" id="xrpUSD" class="form-control stilo" name="spinner">
                            </div>
                        </div>
                        <div class="d-flex flex-row-reverse">
                            <button id="btnCoinsToSell" class="btn btn-warning" type="button">Eladás</button>
                        </div>
                        <script>
                            $("input[name='spinner']").TouchSpin({
                                min: 0,
                                max: 10000,
                                step: 0.0001,
                                decimals: 4,
                                boostat: 1,
                                maxboostedstep: 10,
                            });
                        </script>

                    </div>
                </div>
            </div>
        </div>

        <div class="w-100 d-flex flex-column">
            <div class="w-100 panel panel-primary">
                <div class="panel-heading">Kriptovaluta árfolyamok</div>
                <div id="chartDiv" class="panel-body">
                    <canvas id="canvas"></canvas>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">History</div>
                <div class="panel-body">
                    <table id="tableHistory">
                        <thead id="tableHistoryHead">
                        <tr>
                            <th>Valuta</th>
                            <th>Mennyiség</th>
                            <th>Tranzakció</th>
                            <th>Dátum</th>
                            <th>Árfolyam</th>
                        </tr>
                        </thead>
                        <tbody id="tableHistoryBody">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Offerek</div>
                <div class="panel-body">
                    <table id="tableOffers">

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>